mvn compile -e ; mvn package
cp target/JSP.war ../Deploys/JSP
cd ../Deploys/JSP ; zip ../Archivo-$1-$(date '+%Y-%m-%d-Time-%H').zip * ; cd ..
aws s3 cp Archivo-$1-$(date '+%Y-%m-%d-Time-%H').zip s3://mybucketjsp/
aws elasticbeanstalk create-application-version --application-name JSP --version-label $1 --source-bundle S3Bucket="mybucketjsp",S3Key=Archivo-$1-$(date '+%Y-%m-%d-Time-%H').zip


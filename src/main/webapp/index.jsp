<%@ page import="com.Calculos" %>
<html>
<body>
<h2>Version 3 Dev en Docker!</h2>
Fecha : <%= new java.util.Date() %>
<br>
Sesion : <%= session.getId()%> 
<br>
Request : <%= request.getContentType()%>
<br>

<% 
	int[] naturales={1,2,3,4,5,6,7,8,9,10,11,12};
	for(int i : naturales) out.println("<br> 15 x "+i+" = "+ Calculos.multiplica(15, i) ) ;
	out.println("<br>------------------");
	for(int i : naturales) out.println("<br> 15 + "+i+" = "+ Calculos.suma(15, i) ) ;
	
%>

</body>
</html>
